# Description
This is my submission for homework 5

# Authors
Duncan Shaw

# Copy/Paste Directions
In the terminal type:
  git clone https://:@gitlab.cern.ch:8443/dshaw/Duncan-Shaw-H5.git  
The folder containing this homework assignment will appear in your home directory
To compile the program: While in the file directory containing d1_1.cpp,
type "g++ d1_1.cpp -lm". an executable a.out will appear. Running this executable
will search for the value of alpha from the included dataset. BE WARNED:
THE CODE TAKES A LONG TIME TO RUN. It can take up to half an hour if you have a slow computer.
An output file out.dat will appear. the first collumn is the guess for alpha.
The second collumn is the chi-squared of the value. 
The value of alpha that the code has dertermined should output to the terminal.
In my case the value of alpha was 5.00+-.02, with a chi-squared of ~79.0709. In, fact 
I ran a more accurate version of the code (d1_2.cpp) which produced 4.9992 for alpha,
but I feel like the precision of the previous program was sufficient for the data set.
