#include <fstream>
#include <iostream>
#include <math.h>
/*#include "Riostream.h"
#include "TFile.h"
#include "TH1.h"
#include "TNtuple.h"*/

using namespace std;

float pi= 3.1415926;

//calculate number of lines
int nlines(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	int i=0;
	while(in >> a){
		i++;
	}
	in.close();
	return i;
	}

//-----------------------------
//average value
double average(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	double ave=0;
	while(in >> a){
		ave=ave+a;
	}
	in.close();
	return ave/nlines();	
}

//------------------------------
//minimum value
float min(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	float min=average();
	while(in >> a){
		if (a<min) min=a;
	}
	in.close();
	return min;	
}	

//---------------------------------
//maximum value
float max(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	float max=average();
	while(in >> a){
		if (a>max) max=a;
	}
	in.close();
	return max;	
}	

//---------------------------------
//range of values
float range(){return max()-min();}

//-------------------------------
//lower bound of bin
float a(int binnumber){
	float bin=binnumber;
	return min() + bin * range() / 100;
} 
//upper bound of bin
float b(int binnumber){
	float bin=binnumber;
	return min() + (bin +1) * range() /100;
} 

//--------------------------------
//create an array with 100 values
//each array value is the number of counts in that bin
int *histo(){
	static int hist [100]={0};
	int count;
	float ab;
	int i=0;
	for (int i=0; i<100; i++){
		ifstream in;
		in.open ("data1.dat");
		float min=a(i);
		float max=b(i);
		while (in>>ab){
			if (ab >= min && ab < max){
				hist[i]++;
			}
		}
		in.close();
		cout<<i<<"%"<<endl;
	}
	hist[99]++;
	return hist;
}

//------------------------------------
/*input is the current guess for alpha, the bit to find the difference of,
and the number of counts in that bin.
the output is each term of the sum*/
float diff(float alpha,int bin, int count){
	float expected;
	expected = ( erf(alpha-a(bin)) - erf(alpha-b(bin)) )*(10000/2);
	float sigma=100;
	if (count==0) sigma=expected;
	else sigma=count;
	return pow((count-expected),2) / sigma;	
}

//----------------------------------
//sum each of the terms for a given alpha and returns chi-squared
float chisquared(float alpha,int hist[100]){
	float sum=0;
	for (int i=0;i<100;i++){
		sum=sum+diff(alpha,i,hist[i]);
	}
	return sum;
}

//--------------------------------

int main(){
	cout<<"number of data points= "<<nlines()<<endl;
	cout<<"average= "<<average()<<endl;
	cout<<"minimum value= "<<min()<<endl;
	cout<<"maximum value= "<<max()<<endl;

	int *hist;
	hist=histo();
	int sum=0;
	for (int i=0;i<100;i++)
		sum=sum+*(hist+i);
	cout<<"testing sum= "<<sum<<endl;

	cout<<"diff(50)= "<<diff(5,50,*(hist+50))<<endl;
	cout<<"chi-squared(5)= "<<chisquared(5,hist)<<endl;
	ofstream out1;
	out1.open("out.dat");
	for (float j=0;j<100;j++){
		float alpha=4+j/50;
		out1<<alpha<<" "<<chisquared(alpha,hist)<<endl;
		cout<<"writing... "<<j<<"% complete"<<endl;
	}
	out1.close();

	return 0;
}

//=========================================================================================================
//=========================================================================================================

/*void data(const Char_t* fdata="/home/foxheart/Documents/A5/data1.dat",const Char_t* froot="output.root")
{
	ifstream in;
	in.open(fdata, ios::in);

	Float_t x;
	
	TFile *f =  new TFile(froot, "RECREATE");
	TH1F *h1 = new TH1F("h1", "distribution", 100, 0, 10);
	TNtuple *ntuple = new TNtuple("ntuple", "data from text file","x") ;

	while (in.good()){
		in >> x;
		if (in.good()){
			//fill the histograms and ntuple
			h1-> Fill(x);
		}
	}
	in.close();
	f->Write();
	delete f;
}*/

