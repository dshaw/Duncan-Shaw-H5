#include <fstream>
#include <iostream>
#include <math.h>
/*#include "Riostream.h"
#include "TFile.h"
#include "TH1.h"
#include "TNtuple.h"*/

using namespace std;

float pi= 3.1415926;

//calculate number of lines
int nlines(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	int i=0;
	while(in >> a){
		i++;
	}
	in.close();
	return i;
	}

//-----------------------------
//average value
double average(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	double ave=0;
	while(in >> a){
		ave=ave+a;
	}
	in.close();
	return ave/nlines();	
}

//------------------------------
//minimum value
float min(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	float min=average();
	while(in >> a){
		if (a<min) min=a;
	}
	in.close();
	return min;	
}	

//---------------------------------
//maximum value
float max(){
	ifstream in;
	in.open ("data1.dat");
	float a;
	float max=average();
	while(in >> a){
		if (a>max) max=a;
	}
	in.close();
	return max;	
}	

//---------------------------------
//range of values
float range(){return max()-min();}

//-------------------------------
//lower bound of bin
float a(int binnumber){
	float bin=binnumber;
	return min() + bin * range() / 100;
} 
//upper bound of bin
float b(int binnumber){
	float bin=binnumber;
	return min() + (bin +1) * range() /100;
} 

//--------------------------------
//create an array with 100 values
//each array value is the number of counts in that bin
int *histo(){
	static int hist [100]={0};
	int i=0;
	float ab;
	float min1=min();
	float spread=range();
	ifstream in;
	in.open ("data1.dat");
	while (in>>ab){
		i=( (ab-min1) / spread ) * 100;
		hist[i]++;
	}
	in.close();

	hist[99]++;
	return hist;
}

//------------------------------------
/*input is the current guess for alpha, the bit to find the difference of,
and the number of counts in that bin.
the output is each term of the sum*/
float diff(float alpha,int bin, int count){
	float expected;
	expected = ( erf(alpha-a(bin)) - erf(alpha-b(bin)) )*(10000/2);
	float sigma=100;
	if (count==0) sigma=expected;
	else sigma=count;
	return pow((count-expected),2) / sigma;	
}

//----------------------------------
//sum each of the terms for a given alpha and returns chi-squared
float chisquared(float alpha,int hist[100]){
	float sum=0;
	for (int i=0;i<100;i++){
		sum=sum+diff(alpha,i,hist[i]);
	}
	return sum;
}

//--------------------------------

int main(){
	cout<<"number of data points= "<<nlines()<<endl;
	cout<<"average= "<<average()<<endl;
	cout<<"minimum value= "<<min()<<endl;
	cout<<"maximum value= "<<max()<<endl;

	int *hist;
	hist=histo();

//	some lines for testing functions
//	cout<<"diff(50)= "<<diff(5,50,*(hist+50))<<endl;
//	cout<<"chi-squared(5)= "<<chisquared(5,hist)<<endl;

	ofstream out1;
	out1.open("out.dat");
	float min=50000;
	float minalph=0;
	for (float j=0;j<100;j++){
		float alpha=4+j/50;
		float chi=chisquared(alpha,hist);
		out1<<alpha<<" "<<chi<<endl;
		cout<<"writing... "<<j<<"% complete"<<endl;
		if (chi<min){
			min=chi;
			minalph=j;		
		}
	}
	out1.close();
	cout<<"writing complete"<<endl;
	cout<<"the minimim value of chi-squared was "<<min<<"."<<endl;
	cout<<"it occurs when alpha is "<<minalph<<"."<<endl;

	return 0;
}
